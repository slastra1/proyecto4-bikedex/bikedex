<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider; 
use yii\db\Expression;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function actionConsulta1(){
        
     $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT eq.nomequipo, ROUND(AVG(c.edad)) as promedio_edad FROM equipo eq LEFT JOIN ciclista c ON c.nomequipo = eq.nomequipo GROUP BY eq.nomequipo',
        ]);
        
        return $this->render("grafico1",[
            "dataProvider"=>$dataProvider,
            "campos"=>["nomequipo", "promedio_edad"],
            "titulo"=>"Consulta 1 DAO",
            "enunciado"=>"Promedio de edad de los ciclistas en cada equipo",
            "sql"=>"SELECT eq.nomequipo, ROUND(AVG(c.edad)) as promedio_edad FROM equipo eq LEFT JOIN ciclista c ON c.nomequipo = eq.nomequipo GROUP BY eq.nomequipo"
        ]);          
    }
    
    public function actionConsulta2(){
        
        $dataProvider= new SqlDataProvider([
            'sql'=>'SELECT eq.nomequipo, COUNT(e.numetapa) as etapas_ganadas
            FROM equipo eq
            LEFT JOIN ciclista c ON eq.nomequipo = c.nomequipo
            LEFT JOIN lleva l ON c.dorsal = l.dorsal
            LEFT JOIN etapa e ON l.numetapa = e.numetapa
            WHERE e.dorsal = l.dorsal 
            GROUP BY eq.nomequipo',

        ]); 
        
           return $this->render("grafico2",[
            "dataProvider"=>$dataProvider,
            "campos"=>["nomequipo", "etapas_ganadas"],
            "titulo"=>"Consulta 2 DAO",
            "enunciado"=>"Número total de etapas ganadas por cada equipo",
            "sql"=> "SELECT eq.nomequipo, COUNT(e.numetapa) as etapas_ganadas
            FROM equipo eq
            LEFT JOIN ciclista c ON eq.nomequipo = c.nomequipo
            LEFT JOIN lleva l ON c.dorsal = l.dorsal
            LEFT JOIN etapa e ON l.numetapa = e.numetapa
            WHERE e.dorsal = l.dorsal 
            GROUP BY eq.nomequipo"
        ]);
    }
    
    
    public function actionConsulta3(){
        $dataProvider=new SqlDataProvider([
            'sql'=>'SELECT c.nombre, COUNT(e.numetapa) as total_etapas
            FROM ciclista c
            LEFT JOIN lleva l ON c.dorsal = l.dorsal
            LEFT JOIN etapa e ON l.numetapa = e.numetapa
            GROUP BY c.nombre',
        ]); 
        
        return $this->render("grafico3",[
            "dataProvider"=>$dataProvider,
            "campos"=>["nombre", "total_etapas"],
            "titulo"=>"Consulta 3 DAO",
            "enunciado"=>"Número total de etapas para cada ciclista",
            "sql"=> 'SELECT c.nombre, COUNT(e.numetapa) as total_etapas
            FROM ciclista c
            LEFT JOIN lleva l ON c.dorsal = l.dorsal
            LEFT JOIN etapa e ON l.numetapa = e.numetapa
            GROUP BY c.nombre'
        ]);
    }
    
    /* Para que se renderice la vista y no haga falta hacer un controller */
    public function actionNoticias(){
        return $this->render('noticias');
    }
    
   public function actionGraficos()
{
    // Utiliza SqlDataProvider para obtener resultados directamente de la consulta SQL
    $dataProvider = new SqlDataProvider([
        'sql' => 'SELECT nomequipo, ROUND(AVG(edad)) as promedio_edad FROM ciclista GROUP BY nomequipo',
    ]);

    $dataProvider2 = new SqlDataProvider([
        'sql' => 'SELECT eq.nomequipo, COUNT(e.numetapa) as etapas_ganadas
                  FROM equipo eq
                  LEFT JOIN ciclista c ON eq.nomequipo = c.nomequipo
                  LEFT JOIN lleva l ON c.dorsal = l.dorsal
                  LEFT JOIN etapa e ON l.numetapa = e.numetapa
                  WHERE e.dorsal = l.dorsal 
                  GROUP BY eq.nomequipo',
    ]);

    return $this->render('graficos', [
        'dataProvider' => $dataProvider,
        'dataProvider2' => $dataProvider2, // Asegúrate de pasar ambos DataProviders a la vista
    ]);
}
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionNosotros()
    {
        return $this->render('nosotros');
    }
    public function actionListarCiclistas()
    {
    $ciclistas = Ciclista::find()
        ->select(['nombre'])
        ->groupBy(['nombre'])
        ->asArray()
        ->all();
    return $this->render('listarCiclistas', ['ciclistas' => $ciclistas]);
    }
    public function actionListarNombresCiclistas()
    {
    $nombresCiclistas = Ciclista::find()
        ->select(['nombre'])
        ->groupBy(['nombre'])
        ->asArray()
        ->all();

    // Asegúrate de pasar la variable a la vista
    return $this->renderPartial('listarNombresCiclistas', ['nombresCiclistas' => $nombresCiclistas]);
    }

     public function actionApuestas()
    {
        $resultados = Ciclista::find()
            ->select([
                'c.nombre',
                'COUNT(DISTINCT e.numetapa) as total_etapas', // Utiliza COUNT(DISTINCT) para evitar contar etapas duplicadas
                'COUNT(DISTINCT p.Nompuerto) as total_puertos', // Contar puertos distintos
            ])
            ->from(['c' => 'ciclista'])
            ->leftJoin('lleva l', 'c.dorsal = l.dorsal')
            ->leftJoin('etapa e', 'l.numetapa = e.numetapa')
            ->leftJoin('puerto p', 'e.dorsal = p.dorsal')
            ->groupBy(['c.nombre'])
            ->orderBy(['total_etapas' => SORT_DESC]) // Ordenar por total_etapas de mayor a menor
            ->asArray()
            ->all();

        // Simula los multiplicadores sin afectar la base de datos
        $baseMultiplicadorEtapas = 2;
        $baseMultiplicadorPuertos = 3; 

        $multiplicadores = [];

        foreach ($resultados as $resultado) {
            $totalEtapas = $resultado['total_etapas'];
            $totalPuertos = $resultado['total_puertos'];

            // Ajusta cómo calculas los multiplicadores en base a los totales de etapas y puertos
            $multiplicadores[$resultado['nombre']] = [
                'etapas' => $baseMultiplicadorEtapas / max(1, $totalEtapas),
                'puertos' => $baseMultiplicadorPuertos / max(1, $totalPuertos),
            ];
        }
         $nombresCiclistas = Ciclista::find()
        ->select(['nombre'])
        ->groupBy(['nombre'])
        ->asArray()
        ->all();

        return $this->render('apuestas', [
            'resultados' => $resultados,
            'multiplicadores' => $multiplicadores,
            'nombresCiclistas' => $nombresCiclistas,
        ]);
    }
}
