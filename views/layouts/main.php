<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link href="https://fonts.googleapis.com/css2?family=Dosis&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
        <!-- Integración de Bootstrap CSS -->
        <?= Html::cssFile('https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css') ?>
        <link rel="stylesheet" href="<?=Url::to('@web/css/main.css')?>">
        <?php $this->head() ?>
            
    </head>
    <body class="d-flex flex-column h-100" style="overflow-x: hidden">
        <?php $this->beginBody() ?>

        <header>
            <?php
                NavBar::begin([
                    'brandLabel' => '<img src="' . Yii::getAlias('@web') . '/images/bike.png" alt="Logo" class="img-fluid" style="width:70px; height:70px">',
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar navbar-expand-md navbar-dark bg-dark ',
                    ],
                ]);
                echo Nav::widget([
                'options' => ['class' => 'navbar-nav'],
                'items' => [
                    ['label' => 'Home', 'url' => ['/site/index']],
                    ['label' => 'Noticias', 'url' => ['site/noticias']],
                    ['label' => 'Apuestas', 'url' => ['site/apuestas']],
                    ['label' => 'Graficos', 'url' => ['site/graficos']],
                    ['label' => 'Sobre Nosotros', 'url' => ['/site/nosotros']],
                    [
                        'label' => 'Administración',
                        'items' => [
                            ['label' => 'Ciclistas', 'url' => ['/ciclista/index']],
                            ['label' => 'Equipos', 'url' => ['/equipo/index']],
                            ['label' => 'Etapas', 'url' => ['/etapa/index']],
                            ['label' => 'Llevan', 'url' => ['/lleva/index']],
                            ['label' => 'Maillots', 'url' => ['/maillot/index']],
                            ['label' => 'Puertos', 'url' => ['/puerto/index']],
                        ],
                    ],

                    Yii::$app->user->isGuest ? (
                        ['label' => 'Login', 'url' => ['/site/login']]
                    ) : (
                        '<li>'
                        . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                        . Html::submitButton(
                            'Logout (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>'
                    )
                ],
            ]);
            NavBar::end();
            ?>
        </header>

        <main role="main" class="flex-shrink-0" style="width:100vw; margin:0 ; padding: 0 ">
            <div class="container" style="margin:0; padding : 0; width: 100vw">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </main>

     <footer class="text-light py-1">
    <div class="container">
        <div class="row align-items-center"> <!-- Alinea los elementos verticalmente -->
            <div class="col-md-3 text-center"> <!-- Centro el contenido y elimino la clase d-flex -->
                <img src="<?= Yii::getAlias('@web') ?>../images/bike.png" alt="Logo de la Marca" class="img-fluid rounded-circle" style="width: 160px;">
            </div>

            <div class="col-md-6 text-center"> <!-- Centro el contenido y elimino la clase d-flex -->
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="#" target="_blank"><i style="color:#E9C46A;" class="fab fa-facebook-square fa-2x"></i></a></li>
                    <li class="list-inline-item"><a href="#" target="_blank"><i style="color:#E9C46A;" class="fab fa-twitter-square fa-2x"></i></a></li>
                    <li class="list-inline-item"><a href="#" target="_blank"><i style="color:#E9C46A;" class="fab fa-instagram-square fa-2x"></i></a></li>
                </ul>
            </div>

            <div class="col-md-3 text-md-left"> <!-- Alineo el texto a la izquierda en dispositivos de tamaño medio y superior -->
                <div class="d-flex flex-column align-items-center">
                    <div class="mb-2">
                        <i class="fas fa-map-marker-alt"></i> Dirección: Calle Principal, Ciudad
                    </div>
                    <div>
                        <i class="fas fa-phone-alt"></i> Teléfono: (123) 456-7890
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="footer-text">
            Desarrollado con Grace Hopper.
    </footer>

        <!-- Integración de Bootstrap JS y jQuery -->
        <?= Html::jsFile('https://code.jquery.com/jquery-3.5.1.slim.min.js') ?>
        <?= Html::jsFile('https://cdn.jsdelivr.net/npm/@popperjs/core@2.0.8/dist/umd/popper.min.js') ?>
        <?= Html::jsFile('https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js') ?>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

