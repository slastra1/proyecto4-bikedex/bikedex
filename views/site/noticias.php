<?php
use yii\helpers\Html;
use yii\helpers\Url;
/** @var yii\web\View $this */

$this->title = 'Noticias';
?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <!-- Integración de Bootstrap CSS -->
    <?= Html::cssFile('https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css') ?>
    <link rel="stylesheet" href="<?=Url::to('@web/css/noticias.css')?>">
    <?php $this->head() ?>

</head>
<body>
    <section class="content-header">
    <h1></h1>
    <br>
</section>
    <div class="site-index" style="margin: auto;">
        <div class="body-content">
            <div class="content">
                <div class="wrapper">
                    <div class="content-box">
                        <div class="content-without-middle">
                            <div class="left-side the-article-content">
                                <!-- Encabezado principal de la sección de noticias -->
                                <div class="full-left-box">
                                    <h1 class="specialnews"style=" color: #E9C46A">Noticias de Ciclismo</h1>
                                </div>

                                <!-- Contenido principal de la sección de noticias -->
                                <div class="article-main-content">
                                    <div class="left-article-block article-background">
                                        <div class="article-content">

                                            <!-- Primer bloque de noticias -->
                                            <div class="especialnoticias">
                                                <?= Html::img('@web/images/noticias/aaron.jpg', ['alt' => 'Aaron Gate se impone con claridad en la primera etapa de la Clásica de Nueva Zelanda']) ?>
                                                <h2 class="font-replace"><b>Aaron Gate se impone con claridad en la primera etapa de la Clásica de Nueva Zelanda</b></h2>
                                                <p style="margin-right:5px;margin-left:5px;">El equipo Burgos-BH ha comenzado su temporada 2024 de manera espectacular, con una victoria en la primera etapa de la Clásica de Nueva Zelanda, cortesía de su nuevo fichaje, Aaron... <span class="texto8">(10/01/2024)</span></p>
                                            </div>

                                            <!-- Segundo bloque de noticias -->
                                            <div class="especialnoticias">
                                                <?= Html::img('@web/images/noticias/julian.jpg', ['alt' => 'Julian Alaphilippe confirma que disputará el próximo Giro de Italia y no el Tour']) ?>
                                                <h2 class="font-replace"><b>Julian Alaphilippe confirma que disputará el próximo Giro de Italia y no el Tour</b></h2>
                                                <p style="margin-right:5px;margin-left:5px;">El ciclista francés Julian Alaphilippe no participará en la próxima edición del Tour de Francia y sí en el Giro de Italia, según confirmó al diario L´Equipe. El doble campeón... <span class="texto8">(10/01/2024)</span></p>
                                            </div>

                                            <!-- Tercer bloque de noticias -->
                                            <div class="especialnoticias">
                                                <?= Html::img('@web/images/noticias/philips.jpg', ['alt' => 'Phil Bauhaus vuela en el salto del Lago di Garda y se lleva la victoria']) ?>
                                                <h2 class="font-replace"><b>Phil Bauhaus vuela en el salto del Lago di Garda y se lleva la victoria</b></h2>
                                                <p style="margin-right:5px;margin-left:5px;">El alemán Phil Bauhaus (Bahrain Victorious) se llevó la victoria en la segunda etapa de la Vuelta a Suiza al imponerse al sprint en el salto del Lago di Garda, una llegada... <span class="texto8">(11/01/2024)</span></p>
                                            </div>

                                            <!-- Cuarto bloque de noticias -->
                                            <div class="especialnoticias">
                                                <?= Html::img('@web/images/noticias/dylan.jpg', ['alt' => 'Dylan Groenewegen se impone en la Victoria de las Tres Cantonas']) ?>
                                                <h2 class="font-replace"><b>Dylan Groenewegen se impone en la Victoria de las Tres Cantonas</b></h2>
                                                <p style="margin-right:5px;margin-left:5px;">El ciclista neerlandés Dylan Groenewegen (Team BikeExchange) logró la victoria en la Victoria de las Tres Cantonas, segunda etapa de la Vuelta a Suiza, tras un sprint... <span class="texto8">(10/01/2024)</span></p>
                                            </div>

                                            <!-- Quinto bloque de noticias -->
                                            <div class="especialnoticias">
                                                <?= Html::img('@web/images/noticias/harold.jpg', ['alt' => 'Harold Tejada ficha por el equipo Kern Pharma']) ?>
                                                <h2 class="font-replace"><b>Harold Tejada ficha por el equipo Kern Pharma</b></h2>
                                                <p style="margin-right:5px;margin-left:5px;">El ciclista colombiano Harold Tejada, una de las jóvenes promesas del ciclismo, ha fichado por el equipo español Kern Pharma. Tejada, de 24 años, llega a este equipo... <span class="texto8">(09/01/2024)</span></p>
                                            </div><!-- Primer bloque de noticias -->
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Integración de Bootstrap JS y jQuery -->
    <?= Html::jsFile('https://code.jquery.com/jquery-3.5.1.slim.min.js') ?>
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/@popperjs/core@2.0.8/dist/umd/popper.min.js') ?>
    <?= Html::jsFile('https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js') ?>

</body>
</html>


