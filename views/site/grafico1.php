<?php

use yii\helpers\Html;
use dosamigos\highcharts\HighCharts;

$resultados = $dataProvider->getModels();

$data = [];
foreach ($resultados as $resultado) {
    $data[] = [
        'name' => $resultado['nomequipo'],
        'y' => (int) $resultado['promedio_edad'],
    ];
}

$chartConfig = [
    'chart' => ['type' => 'bar'],
    'title' => ['text' => 'Promedio de Edad por Equipo'],
    'xAxis' => ['categories' => array_column($resultados, 'nomequipo')],
    'yAxis' => ['title' => ['text' => 'Promedio de Edad']],
    'series' => [['name' => 'Promedio de Edad', 'data' => $data]],
];

echo HighCharts::widget(['clientOptions' => $chartConfig]);

echo Html::tag('div', '', ['id' => 'grafico-edad-por-equipo']);

$this->registerJsFile('@web/js/highcharts.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJsFile('@web/js/exporting.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJsFile('@web/js/export-data.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerCssFile('@web/css/highcharts.css');

