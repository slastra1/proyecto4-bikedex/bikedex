<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'Sobre Nosotros';
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
      <link href="https://fonts.googleapis.com/css2?family=Dosis&display=swap" rel="stylesheet">
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">

    <!-- Integración de Bootstrap CSS -->
    <?= Html::cssFile('https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css') ?>

    <?php $this->head() ?>
    <style>
        body {
            font-family: 'Dosis', sans-serif;
            background-color: #264653;
            color: #333;
            margin: 0;
            padding: 0;
            color:white; 
            text-align: center; 
        }

  
        .member {
            
            margin-bottom: 30px;
        }

        .member img {
            width: 150px;
            height: 150px;
            border-radius: 50%;
            margin-bottom: 10px;
        }

        .dynamic-element {
            width: 400px;
            height: 50px;
            position: relative;
            animation: moveElement 2s infinite alternate;
        }
       

        @keyframes moveElement {
            0% {
                transform: translateX(0);
            }
            100% {
                transform: translateX(1060px);
            }
        }
        .member h2 {
    opacity: 0;
}

/* Definición de animaciones */
@keyframes fadeIn {
    0% {
        opacity: 0;
    }
    100% {
        opacity: 1;
    }
}


/* Aplica la animación a los elementos */
.animate__animated {
    animation-duration: 3s;
    animation-fill-mode: both;
}

.animate__fadeIn {
    animation-name: fadeIn;
}

.member {
    flex: 0 0 calc(20% - 20px); /* 20% de ancho para cada miembro con 20px de espacio entre ellos */
    margin-right: 20px; /* Espacio entre los miembros */
    margin-bottom: 20px; /* Espacio debajo de los miembros */
    text-align: center;
}

.member img {
    max-width: 100%; /* Asegura que las imágenes no se desborden del contenedor */
}
    </style>
</head>
<body>
     <header>
        <h1 class="animate__animated animate__fadeIn">Grupo Grace Hopper</h1>
    </header>

    <div class="container" style="display:flex; flex-wrap: wrap; margin: 80px; ">
        <div class="member">
            <img src="<?= Yii::getAlias('@web') ?>../images/nosotros/joselyn.jpeg" alt="Joselyn">
            <h2 class="animate__animated animate__fadeIn">Joselyn</h2>
        </div>

        <div class="member">
            <img src="<?= Yii::getAlias('@web') ?>../images/nosotros/claudia.jpeg"alt="Claudia">
            <h2 class="animate__animated animate__fadeIn">Claudia</h2>
        </div>

        <div class="member">
            <img src="<?= Yii::getAlias('@web') ?>../images/nosotros/mateo.jpeg" alt="Mateo">
            <h2 class="animate__animated animate__fadeIn">Mateo</h2>
        </div>

        <div class="member">
            <img src="<?= Yii::getAlias('@web') ?>../images/nosotros/iker.jpeg" alt="Mateo">
            <h2 class="animate__animated animate__fadeIn">Iker</h2>
        </div>

        <div class="member">
            <img src="<?= Yii::getAlias('@web') ?>../images/nosotros/sergio.jpeg" alt="Sergio">
            <h2 class="animate__animated animate__fadeIn">Sergio</h2>
        </div>
    </div>

    

    <div class="dynamic-element">
        <i class="fas fa-bicycle fa-3x mb-4"></i>
    </div>

    <!-- Integración de Bootstrap JS y jQuery -->
    <?= Html::jsFile('https://code.jquery.com/jquery-3.5.1.slim.min.js') ?>
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/@popperjs/core@2.0.8/dist/umd/popper.min.js') ?>
    <?= Html::jsFile('https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js') ?>
    
</body>
</html>

