
<?php

use yii\helpers\Html;
use yii\helpers\Json;
use dosamigos\highcharts\HighCharts;

$resultados = $dataProvider->getModels();

$data = [];
foreach ($resultados as $resultado) {
    $data[] = [
        'name' => $resultado['nomequipo'],
        'y' => (float) $resultado['etapas_ganadas'],
    ];
}

$chartConfig = [
    'chart' => ['type' => 'bar'],
    'title' => ['text' => 'Número Total de etapas ganadas por Equipo'],
    'xAxis' => ['categories' => array_column($resultados, 'nomequipo')],
    'yAxis' => ['title' => ['text' => 'Total de Etapas ganadas']],
    'series' => [['name' => 'Total de etapas ganadas', 'data' => $data]],
];

echo HighCharts::widget(['clientOptions' => $chartConfig]);

echo Html::tag('div', '', ['id' => 'grafico-edad-por-equipo']);

$this->registerJsFile('@web/js/highcharts.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJsFile('@web/js/exporting.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJsFile('@web/js/export-data.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerCssFile('@web/css/highcharts.css');
