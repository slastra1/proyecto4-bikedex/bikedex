
<?php

use yii\helpers\Html;
use yii\helpers\Json;
use dosamigos\highcharts\HighCharts;

$resultados = $dataProvider->getModels();

$data = [];
foreach ($resultados as $resultado) {
    $data[] = [
        'name' => $resultado['nombre'],
        'y' => (int) $resultado['total_etapas'],
    ];
}

$chartConfig = [
    'chart' => ['type' => 'bar'],
    'title' => ['text' => 'Número total de etapas para cada ciclista'],
    'xAxis' => ['categories' => array_column($resultados, 'nombre')],
    'yAxis' => ['title' => ['text' => 'Número total de etapas para cada ciclista']],
    'series' => [['name' => 'Total de etapas para cada ciclista', 'data' => $data]],
];

echo HighCharts::widget(['clientOptions' => $chartConfig]);

echo Html::tag('div', '', ['id' => 'grafico-etapas-por-ciclista']);

$this->registerJsFile('@web/js/highcharts.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJsFile('@web/js/exporting.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJsFile('@web/js/export-data.js', ['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerCssFile('@web/css/highcharts.css');
