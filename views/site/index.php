
<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'Inicio';
?>

<!-- HTML y CSS Básico -->
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi Sitio - <?= Html::encode($this->title) ?></title>
     <link href="https://fonts.googleapis.com/css2?family=Dosis&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?=Url::to('@web/css/styles.css')?>">
    
<!-- CSS Temporizador -->  
    
<!-- FIN CSS Temporizador -->
</head>
<body>
    <header>
<!-- Destacado y Temporizador -->
<div class="jumbotron" >
    <h1 class="display-4" >Próxima Temporada de Ciclismo</h1>
    <div id="countdown" ></div>
    
<!-- Script JavaScript Temporizador --> 
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            function updateCountdown() {
                var now = new Date().getTime();
                var timeLeft = endDate - now;

                if (timeLeft < 0) {
                    document.getElementById("countdown").innerHTML = "¡La temporada ha comenzado!";
                } else {
                    var days = Math.floor(timeLeft / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((timeLeft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((timeLeft % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((timeLeft % (1000 * 60)) / 1000);

                    document.getElementById("countdown").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";

                    setTimeout(updateCountdown, 1000); // Llama nuevamente después de un segundo
                }
            }

            // Configura la fecha de finalización del temporizador en formato UTC
            var endDate = new Date("April 16, 2024 00:00:00 UTC").getTime();

            // Inicia la actualización del temporizador
            updateCountdown();
        });
    </script>
    
</div>

<!-- Carrusel con Bootstrap -->
<div id="myCarousel" class="carousel slide d-flex align-items-center" data-ride="carousel">
    <div class="carousel-inner" >
        <div class="carousel-item active">
            <div class="carousel-content text-center">
                <img src="<?= Yii::getAlias('@web') ?>../images/bike.png" class="logo-img mb-4" alt="Logo Bikedex">
              
            </div>
            <img style="object-fit:cover" src="<?= Yii::getAlias('@web') ?>../images/imagen13.jpg" class="card-img-top img-fluid" alt="Card 2">
        </div>
        <div class="carousel-item">
            <div class="carousel-content text-center">
                <img src="<?= Yii::getAlias('@web') ?>../images/bike.png" class="logo-img mb-4" alt="Logo Bikedex">
             
            </div>
            <img src="<?= Yii::getAlias('@web') ?>../images/imagen14.jpg" class="card-img-top img-fluid" alt="Card 2">
        </div>
        <!-- Agrega más items del carrusel aquí -->
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Anterior</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Siguiente</span>
    </a>
</div>
    </header>
<main>
<section class="contenedor2 mt-5">
    <div class="container">
    <div class="row">
        <div class="col-md-6">
         <img src="<?= Yii::getAlias('@web') ?>../images/imagen6.jpg" class="card-img-top img-fluid" alt="Card 1">
        </div>
        <div class="col-md-6">
            <p class="titulo">
                NORTE A SUR 
                    por CLUB CICLISMO BIKE DEX</p>
                <p class="descripcion">
Dedicados en cuerpo y alma a preparar un evento ciclista que os deje un sabor de boca inolvidable.
Nuestra prueba consiste en recorrer en autosuficiencia los 999 km  que unen Zaragoza y Sevilla,  con 9.999 de desnivel positivo y con un límite máximo de tiempo de 110 horas. Una experiencia organizada por ciclistas y para ciclistas, para que disfrutes de una aventura sin igual. Ven a atravesar más de seis sierras en un recorrido verdaderamente montañoso.

Te contamos todos los detalles aquí, ¿vamos?
</p>
        </div>
    </div>
</div>
</section>
<section class="contenedor3">
  <h1 class="text-center mb-5 mt-5" style="color: white;">Ciclismo en España</h1>
<div class="container">
  <div class="row">
    <div class="col-md-6 mb-4">
      <div class="imagen-ciclismo">
        <img src="<?= Yii::getAlias('@web') ?>../images/imagen1.jpg" class="card-img-top img-fluid" alt="Card 1">
        <div class="titulo-ciclismo">Ciclismo en Cantabria</div>
      </div>
    </div>
    <div class="col-md-6 mb-4">
      <div class="imagen-ciclismo">
        <img src="<?= Yii::getAlias('@web') ?>../images/imagen2.jpg" class="card-img-top img-fluid" alt="Card 2">
        <div class="titulo-ciclismo">200 km por el campeón Juan Pinar</div>
      </div>
    </div>
    <div class="col-md-6 mb-4">
      <div class="imagen-ciclismo">
        <img src="<?= Yii::getAlias('@web') ?>../images/imagen3.jpg" class="card-img-top img-fluid" alt="Card 3">
        <div class="titulo-ciclismo">Maillot otorgado a Luis Fernandez</div>
      </div>
    </div>
    <div class="col-md-6 mb-4">
      <div class="imagen-ciclismo">
       <img src="<?= Yii::getAlias('@web') ?>../images/imagen4.jpg" class="card-img-top img-fluid" alt="Card 4">
        <div class="titulo-ciclismo">Julio tomó la delantera en ciclosmo 2023</div>
      </div>
    </div>
  </div>
</div>
</section>
 <section class="contenedor4 text-center">
            <h1 class="text-center mb-5 mt-5" style="color: white;">Eventos realizados</h1>
            <div class="container">
                <div class="row">
                   

                    <div class="col-md-4 mb-4">
                        <div class="loading-ball dos" id="participantsCounter">
                            <div class="loading-label">
                                    <i class="fas fa-biking"></i> 
                                <div id="expositoresNumber">0</div>
                                <div>Participantes</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 mb-4">
                        <div class="loading-ball tres" id="talleresCounter">
                            <div class="loading-label">
                                <i class="fas fa-users"></i>
                                <div id="talleresNumber">0</div>
                                <div >Ganadores</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 mb-4">
                        <div class="loading-ball cuatro" id="congresosCounter">
                            <div class="loading-label">
                                <i class="fas fa-route"></i>
                                <div id="congresosNumber">0</div>
                                <div>Rutas</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <section class="contenedor5 pt-5 pb-5">
          <h1 class="text-center mb-5 mt-5" style="color: white;">Galería Fotográfica</h1>
<div class="container">
  <div id="carouselExample" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <!-- Primer grupo de 3 imágenes -->
      <div class="carousel-item active">
        <div class="row">
          <div class="col-md-4"><img src="<?= Yii::getAlias('@web') ?>../images/imagen7.jpg"  class="d-block w-100" alt="Imagen 1"></div>
          <div class="col-md-4"><img src="<?= Yii::getAlias('@web') ?>../images/imagen8.jpg"  class="d-block w-100" alt="Imagen 2"></div>
          <div class="col-md-4"><img src="<?= Yii::getAlias('@web') ?>../images/imagen9.jpg"  class="d-block w-100" alt="Imagen 3"></div>
        </div>
      </div>
      <!-- Segundo grupo de 3 imágenes -->
      <div class="carousel-item">
        <div class="row">
          <div class="col-md-4"><img src="<?= Yii::getAlias('@web') ?>../images/imagen10.jpg" " class="d-block w-100" alt="Imagen 4"></div>
          <div class="col-md-4"><img src="<?= Yii::getAlias('@web') ?>../images/imagen11.jpg"  class="d-block w-100" alt="Imagen 5"></div>
          <div class="col-md-4"><img src="<?= Yii::getAlias('@web') ?>../images/imagen12.jpg"  class="d-block w-100" alt="Imagen 6"></div>
        </div>
      </div>
      <!-- Agrega más grupos de imágenes si es necesario -->
    </div>
    <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Anterior</span>
    </a>
    <a class="carousel-control-next" href="#carouselExample" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Siguiente</span>
    </a>
  </div>
</div>
    </section>
</main>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.0.8/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <script>
   let cont1 = document.getElementById('participantsNumber'),
                cont2 = document.getElementById('expositoresNumber'),
                cont3 = document.getElementById('talleresNumber'),
                cont4 = document.getElementById('congresosNumber');
        let cant1 = 0, cant2 = 0, cant3 = 0, cant4 = 0;
        tiem = 15;

        let tiempo1 = setInterval(() => {
            cont1.textContent = cant1 += 1;
            if (cant1 === 170) {
                clearInterval(tiempo1);
            }
        }, `${tiem}`);

        let tiempo2 = setInterval(() => {
            cont2.textContent = cant2 += 1;
            if (cant2 === 240) {
                clearInterval(tiempo2);
            }
        }, `${tiem}`);

        let tiempo3 = setInterval(() => {
            cont3.textContent = cant3 += 1;
            if (cant3 === 230) {
                clearInterval(tiempo3);
            }
        }, `${tiem}`);

        let tiempo4 = setInterval(() => {
            cont4.textContent = cant4 += 1;
            if (cant4 === 200) {
                clearInterval(tiempo4);
            }
        }, `${tiem}`);
</script>
    </body>
</html>


