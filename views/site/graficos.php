<?php
use dmstr\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use miloschuman\highcharts\Highcharts;
use yii\web\JqueryAsset;

$this->title = 'Graficos';

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Enlace a Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- Agrega enlaces a tus archivos CSS y JavaScript aquí -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <!-- Agrega enlaces a tus archivos CSS -->
    <link rel="stylesheet" href="<?=Url::to('@web/css/main.css')?>">
</head>
<body>
<style>
    .box-title {
        color: #E9C46A;
    }
</style>
<section class="content-header">
    <h1></h1>
    <br>
</section>

<section class="content">
    <?= Alert::widget() ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-success" style="width: 80%; margin: 0 auto; margin-left: 240px;">
                <div class="box-header with-border">
                    <h3 class="box-title">Promedio de edad por equipo</h3>
                </div>
                <div class="box-body">
                    <div class="highchart-box" style="border: 1px solid #ddd; padding: 20px; border-radius: 10px; box-shadow: 0 0 10px rgba(0,0,0,0.1);"> <!-- Estilos CSS mejorados -->
                        <?php
                        // Verifica si $dataProvider está definido
                        if (isset($dataProvider)) {
                            $resultados = $dataProvider->getModels();

                            $data = [];
                            foreach ($resultados as $resultado) {
                                $data[] = [
                                    'name' => $resultado['nomequipo'],
                                    'y' => (int) $resultado['promedio_edad'],
                                ];
                            }

                            $chartConfig = [
                                'chart' => ['type' => 'bar', 'width' => 800], // Ajusta el ancho del gráfico
                                'title' => ['text' => 'Promedio de Edad por Equipo'],
                                'xAxis' => ['categories' => array_column($resultados, 'nomequipo')],
                                'yAxis' => ['title' => ['text' => 'Promedio de Edad']],
                                'plotOptions' => [
                                    'bar' => [
                                        'pointWidth' => 8, // Ajusta el ancho de las barras
                                    ],
                                ],
                                'series' => [
                                    [
                                        'name' => 'Promedio de Edad',
                                        'data' => $data,
                                        'color' => 'red', // Establecer el color rojo
                                    ],
                                ],
                            ];

                            echo Highcharts::widget([
                                'options' => $chartConfig,
                                'scripts' => [
                                    'highcharts-more', // Agrega soporte para gráficos adicionales si es necesario
                                ],
                            ]);

                        } else {
                            echo "Error: DataProvider no está definido.";
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-12" style="margin-top: 20px;"></div>
        
        <div class="col-md-12">
            <div class="box box-success" style="width: 80%; margin: 0 auto; margin-left: 240px;">
                <div class="box-header with-border">
                    <h3 class="box-title">Número Total de etapas ganadas</h3>
                </div>
                <div class="box-body">
                    <div class="highchart-box" style="border: 1px solid #ddd; padding: 20px; border-radius: 10px; box-shadow: 0 0 10px rgba(0,0,0,0.1);"> <!-- Estilos CSS mejorados -->
                    <?php
                    // Verifica si $dataProvider2 está definido para el segundo gráfico
                    if (isset($dataProvider2)) {
                        $resultados2 = $dataProvider2->getModels();
                        // Resto del código para el segundo gráfico
                        $data2 = [];
                        foreach ($resultados2 as $resultado) {
                            if (isset($resultado['etapas_ganadas'])) {
                                $data2[] = [
                                    'name' => $resultado['nomequipo'],
                                    'y' => (float) $resultado['etapas_ganadas'],
                                ];
                            }
                        }

                        $chartConfig2 = [
                            'chart' => ['type' => 'pie', 'width' => 800], // Cambia el tipo de gráfico a pie
                            'title' => ['text' => 'Número Total de etapas ganadas'],
                            'plotOptions' => [
                                'pie' => [
                                    'allowPointSelect' => true,
                                    'cursor' => 'pointer',
                                    'dataLabels' => [
                                        'enabled' => true,
                                        'format' => '<b>{point.name}</b>: {point.y}',
                                    ],
                                ],
                            ],
                            'series' => [
                                [
                                    'name' => 'Total de etapas ganadas',
                                    'data' => $data2,
                                    'colorByPoint' => true,
                                ],
                            ],
                        ];

                        echo Highcharts::widget([
                            'options' => $chartConfig2,
                            'scripts' => [
                                'highcharts-more', // Agrega soporte para gráficos adicionales si es necesario
                            ],
                        ]);

                    } else {
                        echo "Error: DataProvider2 no está definido.";
                    }
                    ?>
                </div>
            </div>
        </div>
    <div class="col-md-12" style="margin-top: 20px;"></div>

        <div class="col-md-12">
            <div class="box box-success" style="width: 80%; margin-bottom: 30px; margin-left: 240px;">
                <div class="box-header with-border">
                    <h3 class="box-title">Total de etapas para cada ciclista</h3>
                </div>
                <div class="box-body">
                    <div class="highchart-box" style="border: 1px solid #ddd; padding: 20px; border-radius: 10px; box-shadow: 0 0 10px rgba(0,0,0,0.1);"> <!-- Estilos CSS mejorados -->
                    <?php
                    $resultados = $dataProvider->getModels();

                    $data = [];
                    foreach ($resultados as $resultado) {
                        if (isset($resultado['nombre'])) {
                            $data[] = [
                                'name' => $resultado['nombre'],
                                'y' => (int) $resultado['total_etapas'],
                            ];
                        }
                    }

                    $chartConfig3 = [
                        'chart' => ['type' => 'bar'],
                        'title' => ['text' => 'Número total de etapas para cada ciclista'],
                        'xAxis' => ['categories' => array_column($resultados, 'nombre')],
                        'yAxis' => ['title' => ['text' => 'Número total de etapas para cada ciclista']],
                        'series' => [['name' => 'Total de etapas para cada ciclista', 'data' => $data]],
                    ];

                    echo HighCharts::widget(['options' => $chartConfig3]);

                    echo Html::tag('div', '', ['id' => 'grafico-etapas-por-ciclista']);
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html> 